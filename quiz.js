let myQuestions = displayQuestions(myQuestion);

(function() {
  let sec = 120,
    countDiv = document.getElementById("timer"),
    countDown = setInterval(function() {
      "use strict";

      secpass();
    }, 1000);
  function secpass() {
    var min = Math.floor(sec / 60),
      remSec = sec % 60;

    if (remSec < 10) {
      remSec = "0" + remSec;
    }
    if (min < 10) {
      min = "0" + min;
    }
    countDiv.innerHTML = min + ":" + remSec;

    if (sec > 0) {
      sec = sec - 1;
    } else {
      clearInterval(countDown);

      countDiv.innerHTML = "Time UP!";

      showResults();
    }
  }

  let mark = 10;

  let questionCounter = 1;

  function buildQuiz() {
    // we'll need a place to store the HTML output
    const output = [];

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // we'll want to store the list of answer choices
      const answers = [];

      // and for each available answer...
      for (letter in currentQuestion.answers) {
        // ...add an HTML radio button
        answers.push(
          `<label>
             <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
           </label>`
        );
      }

      // add this question and its answers to the output
      output.push(
        `<div class="slide">
           <div class="question"> ${currentQuestion.question} </div>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
      );
    });

    // finally combine our output list into one string of HTML and put it on the page
    quizContainer.innerHTML = output.join("");
  }

  function showResults() {
    // gather answer containers from our quiz
    const answerContainers = quizContainer.querySelectorAll(".answers");

    // keep track of user's answers
    let numCorrect = 0;

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // find selected answer
      const answerContainer = answerContainers[questionNumber];
      const selector = `input[name=question${questionNumber}]:checked`;
      const userAnswer = (answerContainer.querySelector(selector) || {}).value;

      // if answer is correct
      if (userAnswer === currentQuestion.correctAnswer) {
        // add to the number of correct answers
        numCorrect++;

        // color the answers green
        answerContainers[questionNumber].style.color = "lightgreen";
      } else {
        // if answer is wrong or blank
        // color the answers red
        answerContainers[questionNumber].style.color = "red";
      }
    });
    // calculate total score
    let computedResult = numCorrect * mark;

    clearInterval(countDown);

    // show number of correct answers out of total
    resultsContainer.innerHTML = `You Answered ${numCorrect} out of ${myQuestions.length} Questions correctly`;

    //Check if score is above pass mark
    if (computedResult >= 75) {
      return (scoreContainer.innerHTML = `<span class='textGreen'>You Passed! You scored ${computedResult}% </span>`);
    } else {
      return (scoreContainer.innerHTML = `<span class='textRed'>Sorry, You scored ${computedResult}% </span>`);
    }
  }

  function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;

    if (currentSlide === 0) {
      previousButton.style.display = "none";
    } else {
      previousButton.style.display = "inline-block";
    }

    if (currentSlide === slides.length - 1) {
      nextButton.style.display = "none";
      submitButton.style.display = "inline-block";
      finishTest.style.display = "block";
    } else {
      nextButton.style.display = "inline-block";
      submitButton.style.display = "none";
      finishTest.style.display = "none";
    }
  }

  function showNextSlide() {
    showSlide(currentSlide + 1);
    questionCounter++;
    counter.innerHTML = `Question ${questionCounter} of ${myQuestions.length}`;
  }

  function showPreviousSlide() {
    showSlide(currentSlide - 1);
    questionCounter--;
    counter.innerHTML = `Question ${questionCounter} of ${myQuestions.length}`;
  }

  const scoreContainer = document.getElementById("score");
  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");
  const finishTest = document.getElementById("finished");

  // display quiz right away
  buildQuiz();
  // countDown();

  const previousButton = document.getElementById("previous");
  const nextButton = document.getElementById("next");
  const slides = document.querySelectorAll(".slide");
  let currentSlide = 0;
  const counter = document.getElementById("counter");

  counter.innerHTML = `Question ${questionCounter} of ${myQuestions.length}`;

  showSlide(0);

  // on submit, show results
  submitButton.addEventListener("click", showResults);
  previousButton.addEventListener("click", showPreviousSlide);
  nextButton.addEventListener("click", showNextSlide);
})();
