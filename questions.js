const myQuestion = [
  {
    question:
      "All of the following are examples of real security and privacy risks EXCEPT:",
    answers: {
      a: "Viruses",
      b: "Hackers",
      c: "Spam",
      d: "Identity theft"
    },
    correctAnswer: "c"
  },
  {
    question: "The term 'Pentium' is related to",
    answers: {
      a: "DVD",
      b: "Hard Disk",
      c: "Microprocessor",
      d: "Mouse"
    },
    correctAnswer: "c"
  },
  {
    question: "What does HTTP stands for?",
    answers: {
      a: "Head Tail Transfer Protocol",
      b: "Hypertext Transfer Protocol",
      c: "Hypertext Transfer Plotter",
      d: "Hypertext Transfer Plot"
    },
    correctAnswer: "b"
  },
  {
    question:
      "The difference between people with access to computers and the Internet and those without this access is known as the:",
    answers: {
      a: "digital divide.",
      b: "Internet divide.",
      c: "cyberway divide.",
      d: "Web divide."
    },
    correctAnswer: "a"
  },
  {
    question:
      "________ controls the way in which the computer system functions and provides a means by which users can interact with the computer.",
    answers: {
      a: "The operating system",
      b: "The motherboard",
      c: "The platform",
      d: "Application software"
    },
    correctAnswer: "a"
  },
  {
    question: "How many generations of computers we have?",
    answers: {
      a: "6",
      b: "7",
      c: "5",
      d: "4"
    },
    correctAnswer: "c"
  },
  {
    question: "Who is father of modern computers?",
    answers: {
      a: "Abraham Lincoln",
      b: "James Gosling",
      c: "Charles Babbage",
      d: "Gordon"
    },
    correctAnswer: "c"
  },
  {
    question: "_______ are software which is used to do particular task.",
    answers: {
      a: "Operating system",
      b: " Program",
      c: "Data",
      d: "Software"
    },
    correctAnswer: "b"
  },
  {
    question: "The most widely used computer device is. ",
    answers: {
      a: "Solid state disks",
      b: "External hard disk",
      c: "Internal hard disk",
      d: "Mouse"
    },
    correctAnswer: "c"
  },
  {
    question:
      "The improvement of computer hardware theory is summarized by which law.?",
    answers: {
      a: "Metcalf's law",
      b: "Bill's Law",
      c: "Moore's First Law",
      d: "Grove's law"
    },
    correctAnswer: "c"
  }
];

function displayQuestions(a) {
  return myQuestion;
}
console.log(displayQuestions(myQuestion));
